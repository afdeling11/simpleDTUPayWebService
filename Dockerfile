FROM adoptopenjdk:11-jre-hotspot
COPY target/lib /usr/src/lib
COPY customer-service/target/customer-service-1.0.0.jar /usr/src
COPY merchant-service/target/merchant-service-1.0.0.jar /usr/src
COPY token-manager-service/target/token-manager-service-1.0.0.jar /usr/src
COPY transaction-service/target/transaction-service-1.0.0.jar /usr/src
WORKDIR /usr/src
COPY target/quarkus-app /usr/src/quarkus-app
CMD java -Xmx64m -jar quarkus-app/quarkus-run.jar
