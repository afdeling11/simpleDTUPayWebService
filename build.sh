#!/bin/bash
set -e

# Build and install the libraries
# abstracting away from using the
# RabbitMq message queue
pushd messaging-utilities
./build.sh
popd

# Build the services
pushd customer-service
./build.sh
popd

pushd merchant-service
./build.sh
popd

pushd token-manager-service
./build.sh
popd

pushd transaction-service
./build.sh
popd