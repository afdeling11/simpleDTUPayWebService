#!/bin/sh
set -e

./build.sh

# End-to-end test
#pushd end-to-end-tests

docker image prune -f
docker-compose up -d rabbitMq
sleep 10
docker-compose up -d customer-service merchant-service token-manager-service transaction-service

docker image prune -f
