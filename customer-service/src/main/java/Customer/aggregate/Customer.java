package Customer.aggregate;

import Customer.events.CustomerCreated;
import Customer.events.CustomerTokenAdded;
import Customer.events.CustomerTokenRemoved;
import Customer.events.Event;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import messaging.Message;

import java.io.Serializable;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

//Rasmus
@Getter
@Setter
public class Customer implements Serializable {

    private static final long serialVersionUID = -14553700082116L;
    private ID id;
    private String accountID;
    private Set<Token> tokens = new HashSet<>();

    @Setter(AccessLevel.NONE)
    private List<Event> appliedEvents = new ArrayList<>();

    private Map<Class<? extends Message>, Consumer<Message>> handlers = new HashMap<>();

    public static Customer create(String accountID){
        ID ID = new ID(UUID.randomUUID());
        CustomerCreated event = new CustomerCreated(ID, accountID);
        Customer customer = new Customer();
        customer.id = ID;
        customer.accountID = accountID;
        customer.appliedEvents.add(event);
        return customer;
    }

    public static Customer createFromEvents(Stream<Event> events){
        Customer customer = new Customer();
        customer.applyEvents(events);
        return customer;
    }

    public Customer() { registerEventHandlers(); }

    private void registerEventHandlers(){
        handlers.put(CustomerCreated.class, e -> apply((CustomerCreated) e));
        handlers.put(CustomerTokenAdded.class, e -> apply((CustomerTokenAdded) e));
        handlers.put(CustomerTokenRemoved.class, e -> apply((CustomerTokenRemoved) e));
        
    }

    /* Business Logic */

    private Token getToken(){
        return getTokens().stream().findFirst().get();
    }

    public void updateAddToken(Set<Token> tokens){
        addTokens(tokens);
        applyEvents(appliedEvents.stream());
    }

    public void updateRemoveToken(Set<Token> tokens) {
        removeToken(tokens);
        applyEvents(appliedEvents.stream());
    }

    private void addTokens(Set<Token> tokens) {
        List<Event> events = tokens.stream()
                .map(token -> (Event)new CustomerTokenAdded(
                        getId(),
                        token))
                .collect(Collectors.toList());
        appliedEvents.addAll(events);
        // add event for this
    }

    private void removeToken(Set<Token> tokens) {
        List<Event> events = tokens.stream().filter(tokens::contains)
                .map(token -> (Event)new CustomerTokenRemoved(
                        getId(),
                        token))
                .collect(Collectors.toList());
        appliedEvents.addAll(events);
        // add event for this
    }

    /* events.Event Handling */

    private void applyEvents(Stream<Event> events) throws Error {
        events.forEachOrdered(this::applyEvent);
        if (this.getId() == null) {
            throw new Error("customer does not exist");
        }
    }

    private void applyEvent(Event e) {
        handlers.getOrDefault(e.getClass(), this::missingHandler).accept(e);
    }

    private void missingHandler(Message e) { throw new Error("handler for "+e+" missing"); }

    private void apply(CustomerCreated event) {
        id = event.getID();
        accountID = event.getAccountID();
    }

    private void apply(CustomerTokenAdded event) {
        Token token = event.getToken();
        tokens.add(token);
    }
    private void apply(CustomerTokenRemoved event) {
        Token token = event.getToken();
        tokens.remove(token);

    }

    public void clearAppliedEvents(){
        appliedEvents.clear();
    }
}
