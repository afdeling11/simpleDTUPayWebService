package Customer.aggregate;

import lombok.AllArgsConstructor;
import lombok.Value;

import java.io.Serializable;
import java.util.UUID;

//Rasmus
@Value
@AllArgsConstructor
public class ID implements Serializable {
    private static final long serialVersionUID = -1455308747700082116L;
    UUID uuid;
}
