package Customer.aggregate;

import lombok.Getter;

import java.io.Serializable;

//Nicklas
@Getter
public class Token implements Serializable {
    private static final long serialVersionUID = -8747700082116L;
    private String token;
}
