package Customer.events;

import Customer.aggregate.ID;
import Customer.aggregate.Token;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Value;

//Marie
@Value
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class CustomerTokenRemoved extends Event{

    private static final long serialVersionUID = 1596683920706802940L;
    private ID id;
    private Token token;

    @Override
    public ID getID() {
        return id;
    }
}
