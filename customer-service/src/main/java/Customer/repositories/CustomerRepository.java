package Customer.repositories;

import Customer.aggregate.Customer;
import Customer.aggregate.ID;
import messaging.MessageQueue;

//Nicklas
public class CustomerRepository {
    private EventStore eventStore;

    public CustomerRepository(MessageQueue messageQueue) {
        this.eventStore = new EventStore(messageQueue);
    }

    public Customer getCustomerById(ID id){
        return Customer.createFromEvents(eventStore.getEventsFor(id));
    }

    public void save(Customer customer){
        eventStore.addEvents(customer.getId(),customer.getAppliedEvents());
        customer.clearAppliedEvents();
    }

    public void remove(ID id) {
        eventStore.removeEvents(id);
    }
}
