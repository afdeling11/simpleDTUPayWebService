package Customer.services;

import Customer.aggregate.*;
import Customer.repositories.CustomerRepository;
import Customer.repositories.ReadCustomerModelRepository;
import messaging.MessageQueue;
import messaging.Event;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.*;

public class CustomerService {

    /* Events */
    public static final String CUSTOMER_REGISTRATION_REQUESTED = "CustomerRegistrationRequested";
    public static final String CUSTOMER_TOKENS_GENERATED = "CustomerTokensGenerated";
    public static final String CUSTOMER_TRANSACTIONS_REQUESTED = "CustomerTransactionsRequested";
    public static final String CUSTOMER_TRANSACTIONS_REPORT_GENERATED = "CustomerTransactionsReportGenerated";
    public static final String CUSTOMER_REQUEST_NEW_TOKENS = "CustomerRequestNewTokens";
    private MessageQueue queue;
    private Map<CorrelationId, CompletableFuture<SubCustomer>> correlations = new ConcurrentHashMap<>();
    private Map<CorrelationId, CompletableFuture<Set<Transaction>>> correlationsTransaction = new ConcurrentHashMap<>();

    private CustomerRepository repository;
    private ReadCustomerModelRepository readCustomerModelRepository;

    public CustomerService(CustomerRepository repository, ReadCustomerModelRepository readCustomerModelRepository, MessageQueue q) {
        this.repository = repository;
        this.readCustomerModelRepository = readCustomerModelRepository;
        this.queue = q;
        //queue.addHandler(CUSTOMER_REGISTRATION_REQUESTED, this::createCustomer);
        queue.addHandler(CUSTOMER_TOKENS_GENERATED, this::updateAddToken);
        queue.addHandler(CUSTOMER_TRANSACTIONS_REPORT_GENERATED, this::transactionReportGenerated);
    }

    /* Command operations */

    public Customer createCustomer(String accountId){
        var correlationId = CorrelationId.randomId();
        correlations.put(correlationId, new CompletableFuture<>());

        Customer customer = Customer.create(accountId);
        SubCustomer sc = new SubCustomer();
        sc.setId(customer.getId());
        sc.setAccountID(customer.getAccountID());
        sc.setTokens(customer.getTokens());

        Event event = new Event(CUSTOMER_REGISTRATION_REQUESTED, new Object[] { sc, correlationId, 5 });

        queue.publish(event);

        repository.save(customer);

        sc = correlations.get(correlationId).join();

        customer.setId(sc.getId());
        customer.setAccountID(sc.getAccountID());
        customer.setTokens(sc.getTokens());

        return customer;
    }
    public Set<Token> requestNewTokens(ID id, int amount){
        Customer customer = repository.getCustomerById(id);
        CorrelationId correlationId = CorrelationId.randomId();
        SubCustomer subCustomer = new SubCustomer();
        subCustomer.setTokens(customer.getTokens());
        subCustomer.setAccountID(customer.getAccountID());
        subCustomer.setId(customer.getId());
        Event event = new Event(CUSTOMER_REQUEST_NEW_TOKENS, new Object[]{subCustomer, correlationId, amount});
        queue.publish(event);
        repository.save(customer);

        subCustomer = correlations.get(correlationId).join();

        customer.setId(subCustomer.getId());
        customer.setAccountID(subCustomer.getAccountID());
        customer.setTokens(subCustomer.getTokens());

        return customer.getTokens();
    }

    // Convert to event, so the transactions can be removed from transaction service
    public void deregisterCustomer(ID id) {
        repository.remove(id);
    }

    public void updateAddToken(Event e){//ID id, Set<Token> tokens
        var c = e.getArgument(0, SubCustomer.class);
        var correlationId = e.getArgument(1, CorrelationId.class);

        Customer customer = repository.getCustomerById(c.getId());
        customer.updateAddToken(c.getTokens());

        repository.save(customer);

        correlations.get(correlationId).complete(c);
    }

    // customer gets his transactions

    public Set<Transaction> requestTransactions(ID id) {
        var correlationId = CorrelationId.randomId();
        correlationsTransaction.put(correlationId, new CompletableFuture<>());
        Event event = new Event(CUSTOMER_TRANSACTIONS_REQUESTED, new Object[] { new HashSet<Transaction>(), correlationId, id });
        queue.publish(event);
        return correlationsTransaction.get(correlationId).join();
    }

    public Set<Transaction> transactionReportGenerated(Event e) {
        Set<Transaction> t = (Set<Transaction>) e.getArguments()[0];
        var correlationId = e.getArgument(1, CorrelationId.class);
        correlationsTransaction.get(correlationId).complete(t);
        return t;
    }

    public void updateRemoveToken(ID id, Set<Token> tokens){
        Customer customer = repository.getCustomerById(id);
        customer.updateRemoveToken(tokens);

        repository.save(customer);
    }

    /* Query Operations */

    public Set<Token> token(ID id){
        return Stream.of(readCustomerModelRepository.getToken(id)).collect(Collectors.toSet());
    }

    public Set<Token> tokens(ID id){
        return readCustomerModelRepository.getTokens(id);
    }


}
