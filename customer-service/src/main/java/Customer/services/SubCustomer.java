package Customer.services;

import Customer.aggregate.ID;
import Customer.aggregate.Token;
import lombok.Data;

import java.io.Serializable;
import java.util.Set;

//Marie
@Data
public class SubCustomer implements Serializable {

    private static final long serialVersionUID = 3L;
    private ID id;
    private String accountID;
    private Set<Token> tokens;
 }
