package Customer.services;

import Customer.aggregate.ID;
import Customer.aggregate.Token;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

//Nicklas
@Data
public class Transaction implements Serializable {

    private static final long serialVersionUID = -14553087476L;
    private ID id;
    private Token token;
    private String merchantID;
    private int amount;
    private String description;
}
