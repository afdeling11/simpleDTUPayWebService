package Merchant.aggregate;

import lombok.Value;

import java.util.UUID;

//Ida
@Value
public class CorrelationId {
    private UUID id;

    public static CorrelationId randomId(){
        return new CorrelationId(UUID.randomUUID());
    }
}
