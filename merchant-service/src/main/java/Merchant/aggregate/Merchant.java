package Merchant.aggregate;

import Merchant.events.Event;
import Merchant.events.MerchantCreated;
import lombok.*;
import messaging.Message;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Stream;

//Ida
@Data
public class Merchant {

    private ID id;
    private String accountID;

    @Setter(AccessLevel.NONE)
    private List<Event> appliedEvents = new ArrayList<>();

    private Map<Class<? extends Message>, Consumer<Message>> handlers = new HashMap<>();

    public static Merchant create(String accountId) {
        ID id = new ID(UUID.randomUUID());
        MerchantCreated event = new MerchantCreated(id, accountId);
        Merchant merchant = new Merchant();
        merchant.id = id;
        merchant.accountID = accountId;
        merchant.appliedEvents.add(event);
        return merchant;
    }

    public static Merchant createFromEvents(Stream<Event> events){
        Merchant merchant = new Merchant();
        merchant.applyEvents(events);
        return merchant;
    }

    public Merchant() { registerEventHandlers(); }

    private void registerEventHandlers(){
        handlers.put(MerchantCreated.class, e -> apply((MerchantCreated) e));
    }

    /* Business Logic */

    /* events.Event Handling */
    private void applyEvents(Stream<Event> events) throws Error {
        events.forEachOrdered(this::applyEvent);
        if (this.getId() == null) {
            throw new Error("merchant does not exist");
        }
    }

    private void applyEvent(Event e) { handlers.getOrDefault(e.getClass(), this::missingHandler).accept(e); }

    private void missingHandler(Message e) { throw new Error("handler for "+e+" missing"); }

    private void apply(MerchantCreated event) {
        id = event.getID();
        accountID = event.getAccountID();
    }

    public void clearAppliedEvents(){
        appliedEvents.clear();
    }
}
