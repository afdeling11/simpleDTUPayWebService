package Merchant.aggregate;

import Merchant.aggregate.ID;
import Merchant.aggregate.Token;
import lombok.Data;
import lombok.Setter;

//Ida
@Data
public class MerchantTransaction {

    private ID id;
    private Token token;
    private String merchantID;
    private int amount;
    private String description;


}
