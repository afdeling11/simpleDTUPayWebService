package Merchant.events;

import Merchant.aggregate.ID;
import lombok.*;

//Hans
@Value
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class MerchantCreated extends Event{

    private static final long serialVersionUID =  -1599019626118724483L;
    ID merchantID;
    String AccountID;

    @Override
    public ID getID() {
        return merchantID;
    }
}
