package Merchant.repositories;

import Merchant.events.MerchantCreated;
import messaging.MessageQueue;

//Marie
public class ReadMerchantModelRepository {

    public ReadMerchantModelRepository(MessageQueue messageQueue) {
        messageQueue.addHandler(MerchantCreated.class, e -> apply((MerchantCreated) e));
    }

    public void apply(MerchantCreated event) {

    }

}
