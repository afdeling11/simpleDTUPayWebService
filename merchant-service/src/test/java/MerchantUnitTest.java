import Merchant.aggregate.*;
import Merchant.repositories.MerchantRepository;
import Merchant.repositories.ReadMerchantModelRepository;
import Merchant.services.MerchantService;
import messaging.Event;
import messaging.Message;
import messaging.MessageQueue;
import messaging.implementations.MessageQueueSync;
import org.junit.Test;
import org.junit.Assert;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

import static org.mockito.Mockito.mock;



//Mathias
public class MerchantUnitTest {
    MessageQueue queue = mock(MessageQueue.class);
    MerchantRepository merchantRepository;
    MerchantService merchantService;


    Merchant merchant;
    Token token = new Token();
    ID id = new ID(UUID.randomUUID());
    ID transactionId = new ID(UUID.randomUUID());
    MerchantTransaction transaction;
    CorrelationId correlationId;

    private void setup_sync_queues() {
        MessageQueue eventQueue = new MessageQueueSync();
        merchantRepository = new MerchantRepository(eventQueue);
        ReadMerchantModelRepository readRepository = new ReadMerchantModelRepository(eventQueue);
        merchantService = new MerchantService(merchantRepository, readRepository, queue);
    }

    public void applicationTest() throws InterruptedException {
        correlationId = CorrelationId.randomId();
        merchant = new Merchant();
        merchant.setAccountID("123456");
        merchant.setId(id);

        transaction = new MerchantTransaction();
        transaction.setAmount(2);
        transaction.setMerchantID(merchant.getAccountID());
        transaction.setDescription("test transaction");
        transaction.setToken(token);
        transaction.setId(transactionId);

        merchantService.initiateTransaction(transactionId, transaction.getToken(), transaction.getDescription(), transaction.getAmount());

        Set<MerchantTransaction> merchantTransactions = merchantService.requestTransactions();

        merchantTransactions.size();


    }

    @Test
    public void runTests() throws InterruptedException {
        setup_sync_queues();
        //applicationTest();
    }

}
