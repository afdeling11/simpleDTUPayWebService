package behaviourtests;


import Merchant.aggregate.*;
import Merchant.repositories.MerchantRepository;
import Merchant.repositories.ReadMerchantModelRepository;
import Merchant.services.MerchantService;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.junit.CucumberOptions;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

import messaging.Event;
import messaging.MessageQueue;
import org.junit.platform.suite.api.ConfigurationParameter;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

//Marie
@CucumberOptions(glue = "")
@ConfigurationParameter(key = "GLUE_PROPERTY_NAME", value = "com.example.application")
public class MerchantSteps {

    private Map<String, CompletableFuture<Event>> publishedEvents = new HashMap<>();

    MessageQueue queue = mock(MessageQueue.class);

    Merchant merchant;
    MerchantTransaction transaction;
    MerchantRepository merchantRepository = new MerchantRepository(queue);
    ReadMerchantModelRepository readMerchantModelRepository = new ReadMerchantModelRepository(queue);
    MerchantService mservice = new MerchantService(merchantRepository, readMerchantModelRepository, queue);
    Token token = new Token();
    ID id = new ID(UUID.randomUUID());

    private Map<Merchant, CorrelationId> correlationIds = new HashMap<>();


    @Given("a merchant with account id {string}")
    public void aMerchantWithAccountId(String id) {
        merchant = Merchant.create(id);
    }

    @And("a customer token is provided")
    public void aCustomerTokenIsProvided() {
        token.setToken("1234");
    }

    @And("the merchant initiates a payment")
    public void theMerchantInitiatesAPayment() {
        transaction = new MerchantTransaction();
        transaction.setMerchantID("testMerchantID");
        transaction.setAmount(50);
        transaction.setDescription("Test description");
        transaction.setToken(token);
        transaction.setId(id);


    }

    @When("the payment is getting processed")
    public void thePaymentIsGettingProcessed() {
        Merchant m = new Merchant();
        mservice.handleMerchantTransaction(new Event(MerchantService.MERCHANT_TRANSACTION_INITIATED, new Object[]
                {merchant,correlationIds.get(merchant)}));
    }

    @Then("the {string} event is published")
    public void theEventIsPublished(String event) {
        Event e = publishedEvents.get(merchant.getAccountID()).join();
        assertEquals(event,e.getType());
        Merchant m = e.getArgument(0,Merchant.class);
        CorrelationId correlationId = e.getArgument(1,CorrelationId.class);
        correlationIds.put(m, correlationId);
    }

    @When("the {string} is received non empty")
    public void theIsReceivedNonEmpty(String event) {
        assertNotNull(event);
    }

    @Then("the transaction is registered in the system")
    public void theTransactionIsRegisteredInTheSystem() {

    }
}
