package messaging;

import java.util.function.Consumer;

public interface MessageQueue {

	void publish(Event message);
	void publish(Message message);
	void addHandler(String eventType, Consumer<Event> handler);
	void addHandler(Class<? extends Message> event, Consumer<Message> handler);

}
