import dtu.ws.fastmoney.*;

import java.util.List;

public class main {

    public static void main(String[] args) {
        BankService bank = new BankServiceService().getBankServicePort();

        /*
        try {
            id = bank.createAccountWithBalance(user, new BigDecimal(0));
            System.out.println(id);
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
        }*/
        try {
            List<AccountInfo> accountInfoList = bank.getAccounts();
            for (AccountInfo ac : accountInfoList) {
                System.out.println(ac.getAccountId() + " | " + ac.getUser().getLastName());
                if (ac.getUser().getLastName().equals("123") || ac.getUser().getLastName().equals("456")) {
                    bank.retireAccount(ac.getAccountId());
                }
            }
        }catch (Exception e){ //todo
            System.out.println(e);
        }
    }
}