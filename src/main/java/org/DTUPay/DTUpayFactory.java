package org.DTUPay;

import Customer.repositories.CustomerRepository;
import Customer.repositories.ReadCustomerModelRepository;
import Customer.services.CustomerService;
import Merchant.repositories.MerchantRepository;
import Merchant.repositories.ReadMerchantModelRepository;
import Merchant.services.MerchantService;
import Transaction.repositories.ReadTransactionModelRepository;
import Transaction.repositories.TransactionRepository;
import Transaction.services.TransactionService;
import lombok.Getter;
import messaging.MessageQueue;
import messaging.implementations.MessageQueueSync;
import messaging.implementations.RabbitMqQueue;

//Rasmus s185119
@Getter
public class DTUpayFactory {
    static DTUpayFactory service = null;
    CustomerService customerService = null;
    MerchantService merchantService = null;
    TransactionService transactionService = null;

    private DTUpayFactory (CustomerService customerService, MerchantService merchantService, TransactionService transactionService) {
        this.customerService = customerService;
        this.merchantService = merchantService;
        this.transactionService = transactionService;
    }

    public static DTUpayFactory getService() {
        if(service != null) return service;
        var messageQueueSync = new MessageQueueSync();
        var mq = new RabbitMqQueue("rabbitMq");
        CustomerRepository customerRepository = new CustomerRepository(messageQueueSync);
        ReadCustomerModelRepository readCustomerModelRepository = new ReadCustomerModelRepository(messageQueueSync);
        CustomerService customerService = new CustomerService(customerRepository, readCustomerModelRepository, mq);

        MerchantRepository merchantRepository = new MerchantRepository(messageQueueSync);
        ReadMerchantModelRepository merchantModelRepository = new ReadMerchantModelRepository(messageQueueSync);
        MerchantService merchantService = new MerchantService(merchantRepository, merchantModelRepository, mq);

        TransactionRepository transactionRepository = new TransactionRepository(messageQueueSync);
        ReadTransactionModelRepository readTransactionModelRepository = new ReadTransactionModelRepository(messageQueueSync);
        TransactionService transactionService = new TransactionService(transactionRepository, readTransactionModelRepository, mq);


        service = new DTUpayFactory(customerService, merchantService, transactionService);
        return service;
    }



}
