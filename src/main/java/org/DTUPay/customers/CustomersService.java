package org.DTUPay.customers;

import Customer.aggregate.Customer;
import Customer.aggregate.ID;
import Customer.aggregate.Token;
import Customer.repositories.CustomerRepository;
import Customer.repositories.ReadCustomerModelRepository;
import Customer.services.CustomerService;
import Customer.services.Transaction;
import messaging.MessageQueue;
import messaging.implementations.MessageQueueSync;
import org.DTUPay.DTUpayFactory;
import org.DTUPay.businesslogic.legacy.Models.DPCustomer;
import org.jboss.logging.annotations.Param;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Set;

// Hans s185110
@Path("/customers")
public class CustomersService {

    //Register
    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    public Customer CreateAccount (String accountid){
        return DTUpayFactory.getService().getCustomerService().createCustomer(accountid);
    }


    //POST /{id}
    //DeRegister
    @Path("/{id}")
    @DELETE
    @Consumes(MediaType.TEXT_PLAIN)
    public void DeRegister(@PathParam("id")ID id){
        DTUpayFactory.getService().getCustomerService().deregisterCustomer(id);
    }
    //GET {id}/tokens
    //GetTokens
    @Path("/{id}/tokens")
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    public Set<Token> GetTokens(@PathParam("id") ID id) {
        return DTUpayFactory.getService().getCustomerService().tokens(id);
    }

    //POST {id}/tokens
    //RequestTokens
    @Path("/{id}/tokens")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Set<Token> RequestTokens(@PathParam("id") ID id, @QueryParam("amount") int amount)
    {
        return DTUpayFactory.getService().getCustomerService().requestNewTokens(id, amount);
    }

    //GET {id}/report
    //GetReport
    @Path("/{id}/report")
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    public Set<Transaction> GetReport(@PathParam("id") ID id) {
        return DTUpayFactory.getService().getCustomerService().requestTransactions(id);
    }
}
