package org.DTUPay.merchants;

import Merchant.aggregate.ID;
import Merchant.aggregate.MerchantTransaction;
import Merchant.aggregate.Token;
import dtu.ws.fastmoney.Transaction;
import org.DTUPay.DTUpayFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Set;
// Hans s185110
@Path("/merchants")
public class MerchantsService {

    //POST
    //Register
    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    public String Register(String accountId){
        return DTUpayFactory.getService().getMerchantService().createMerchant(accountId).toString();
    }

    //POST /{id}
    //DeRegister
    @Path("/{id}")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void DeRegister(@PathParam("id") ID id){
        DTUpayFactory.getService().getMerchantService().deregisterMerchant(id);
    }

    //POST {id}/payment
    //Pay
    @Path("/{id}/payment")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public MerchantTransaction Pay(@PathParam("id") ID id, @QueryParam("token") Token token, @QueryParam("amount") int amount, @QueryParam("description") String description) {
        return DTUpayFactory.getService().getMerchantService().initiateTransaction(id, token, description, amount);
    }
    //GET {id}/report
    //GetReport
    @Path("/{id}/report")
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    public Set<MerchantTransaction> GetReport(@PathParam("id") ID id){
        return DTUpayFactory.getService().getMerchantService().requestTransactions();
    }
}
