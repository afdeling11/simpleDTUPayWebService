package org.DTUPay.reports;
import Transaction.aggregate.Transaction;
import org.DTUPay.DTUpayFactory;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import java.util.Set;

// Hans s185110
@Path("/reports")
public class ReportsService {

    //GET
    //GetReport
    @GET
    public Set<Transaction> GetReport(){
        return DTUpayFactory.getService().getTransactionService().transactions();
    }
}
