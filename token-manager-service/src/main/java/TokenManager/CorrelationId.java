package TokenManager;

import lombok.Value;

import java.util.UUID;

//Hans
@Value
public class CorrelationId {
    private UUID id;

    public static CorrelationId randomId() {
        return new CorrelationId(UUID.randomUUID());
    }
}
