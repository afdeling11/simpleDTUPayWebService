package TokenManager;

import lombok.Data;

import java.util.HashSet;
import java.util.Set;

//Hans
@Data
public class Customer {

    private ID id;
    private String accountID;
    private Set<Token> tokens = new HashSet<>();

}
