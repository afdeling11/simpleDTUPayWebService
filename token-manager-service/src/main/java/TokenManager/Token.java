package TokenManager;

import lombok.Getter;

import java.security.SecureRandom;


//Rasmus
@Getter
public class Token {
    private String token;
    static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    static SecureRandom rnd = new SecureRandom();

    public Token() {
        token = generateRandomString(15);
    }

    private String generateRandomString(int len) {
        StringBuilder sb = new StringBuilder(len);
        for(int i = 0; i < len; i++)
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        return sb.toString();
    }

}
