import TokenManager.CorrelationId;
import TokenManager.TokenManager;
import TokenManager.Token;
import TokenManager.Customer;
import messaging.Event;
import messaging.MessageQueue;
import messaging.implementations.MessageQueueSync;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class TokenUnitTest {
    MessageQueue queue = mock(MessageQueue.class);
    Customer customer;
    Customer expected;
    TokenManager tokenManager = new TokenManager(queue);
    private CorrelationId correlationId;

    public void applicationTest() throws InterruptedException {
        correlationId = CorrelationId.randomId();
        customer = new Customer();
        Assert.assertEquals( new HashSet<Token>(), customer.getTokens());
        tokenManager.handleTokenRequest(new Event(TokenManager.CUSTOMER_REGISTRATION_REQUESTED, new Object[]{customer, correlationId, 5}));
        Thread.sleep(1000);

        expected = new Customer();

        expected.setTokens(tokenManager.requestTokens(expected.getTokens(), 5));
        var event = new Event(TokenManager.CUSTOMER_TOKENS_GENERATED, new Object[] {expected, correlationId});
        //verify(queue).publish(event);

        Assert.assertEquals( 5, expected.getTokens().size());
    }

    @Test
    public void runTests() throws InterruptedException {
        applicationTest();
    }
}
