package Transaction.aggregate;

import lombok.Data;
import lombok.Getter;

import java.io.Serializable;
//Nicklas
@Data
public class SubTransaction implements Serializable {
    private static final long serialVersionUID = 167895678934567L;
    private ID id;
    private Token token;
    private String merchantID;
    private int amount;
    private String description;

}
