package Transaction.aggregate;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.security.SecureRandom;

//Nicklas
@Getter
@Setter
public class Token implements Serializable {
    private static final long serialVersionUID = 2L;
    private String token;
    private String customerId;
}
