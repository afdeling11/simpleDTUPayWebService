package Transaction.aggregate;

import Transaction.events.Event;
import Transaction.events.TransactionCreated;
import lombok.*;
import messaging.Message;

import java.io.Serializable;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Stream;


//Nicklas
@Getter
@Setter
public class Transaction implements Serializable {

    private static final long serialVersionUID = 1L;
    private ID id;
    private Token token;
    private String merchantID;
    private int amount;
    private String description;

    @Setter(AccessLevel.NONE)
    private List<Event> appliedEvents = new ArrayList<>();


    private Map<Class<? extends Message>, Consumer<Message>> handlers = new HashMap<>();

    public static Transaction create(ID id, Token token, String merchantId, int amount, String description){
        TransactionCreated event = new TransactionCreated(id,token,merchantId,amount,description );
        Transaction transaction = new Transaction();
        transaction.token = token;
        transaction.merchantID = merchantId;
        transaction.amount = amount;
        transaction.description = description;
        transaction.appliedEvents.add(event);
        return transaction;
    }

    public static Transaction createFromEvents(Stream<Event> events){
        Transaction transaction = new Transaction();
        transaction.applyEvents(events);
        return transaction;
    }

    public Transaction(){
        registerEventHandlers();
    }

    private void registerEventHandlers(){
        handlers.put(TransactionCreated.class, e -> apply((TransactionCreated) e));
    }

    /* Business Logic */
    //TODO: full list of all transactions

    /* events.Event Handler */
    private void applyEvents(Stream<Event> events) throws Error{
        events.forEachOrdered(this::applyEvent);
        if(this.token == null){
            throw new Error("transaction does not exist");
        }
    }

    private void applyEvent(Event e){
        handlers.getOrDefault(e.getClass(), this::missingHandler).accept(e);
    }

    private void missingHandler(Message e) { throw new Error("handler for "+e+" missing"); }

    private void apply(TransactionCreated event){
        token = event.getToken();
        merchantID = event.getMerchantId();
        amount = event.getAmount();
        description = event.getDescription();
    }

    public void clearAppliedEvents(){
        appliedEvents.clear();
    }
}
