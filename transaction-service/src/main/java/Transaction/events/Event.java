package Transaction.events;

import lombok.Getter;
import messaging.Message;

import java.io.Serializable;

//Rasmus
public abstract class Event implements Message, Serializable {
    private static final long serialVersionUID = -8371089289505091780L;

    private static long versionCount = 1;

    @Getter
    private final long version = versionCount++;

}
