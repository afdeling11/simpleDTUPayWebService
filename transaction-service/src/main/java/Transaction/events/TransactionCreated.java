package Transaction.events;

import Transaction.aggregate.ID;
import Transaction.aggregate.Token;
import lombok.*;
//Rasmus
@Value
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class TransactionCreated extends Event {

    private static final long serialVersionUID = 6472986364743399971L;

    private ID id;
    private Token token;
    private String merchantId;
    private int amount;
    private String description;

}
