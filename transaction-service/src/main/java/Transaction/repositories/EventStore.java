package Transaction.repositories;

import Transaction.events.Event;
import lombok.NonNull;
import messaging.MessageQueue;
import Transaction.aggregate.ID;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

//Mathias
public class EventStore {
    private Map<ID, List<Event>> store = new ConcurrentHashMap<>();

    private MessageQueue eventBus;

    public EventStore(MessageQueue bus) {
        this.eventBus = bus;
    }

    public void addEvent(ID id, Event event) {
        if (!store.containsKey(id)) {
            store.put(id, new ArrayList<>());
        }
        store.get(id).add(event);
        eventBus.publish(event);
    }

    public Stream<Event> getEventsFor(ID id) {
        if (!store.containsKey(id)) {
            store.put(id, new ArrayList<>());
        }
        return store.get(id).stream();
    }

    public void addEvents(@NonNull ID id, List<Event> appliedEvents) {
        appliedEvents.forEach(e -> addEvent(id, e));
    }

    public void removeEvents(ID id) {
        store.remove(id);
    }
}
