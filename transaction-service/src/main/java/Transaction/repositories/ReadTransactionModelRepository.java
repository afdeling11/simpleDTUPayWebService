package Transaction.repositories;

import Transaction.aggregate.ID;
import Transaction.aggregate.Transaction;
import Transaction.events.TransactionCreated;
import messaging.MessageQueue;

import java.util.*;

//Mathias
public class ReadTransactionModelRepository {

    private Map<ID, Set<Transaction>> transactions = new HashMap<>();

    public ReadTransactionModelRepository(MessageQueue eventQueue){
        eventQueue.addHandler(TransactionCreated.class, e -> apply((TransactionCreated) e));
    }

    public Set<Transaction> getTransactionsById(ID id){
        return new HashSet<>(transactions.getOrDefault(id, new HashSet<>()));
    }
    
    public Set<Transaction> getTransactions(){
        Set<Transaction> set = new HashSet<>();
        for (ID id : transactions.keySet()) {
            set.addAll(transactions.get(id));
        }
        return set;
    }

    public void apply(TransactionCreated event){
        Set<Transaction> set = transactions.getOrDefault(event.getId(), new HashSet<>());
        Transaction transaction = new Transaction();
        transaction.setToken(event.getToken());
        transaction.setMerchantID(event.getMerchantId());
        transaction.setAmount(event.getAmount());
        transaction.setDescription(event.getDescription());

        set.add(transaction);
        transactions.put(event.getId(),set);
    }
}
