package Transaction.services;

import dtu.ws.fastmoney.*;

import java.math.BigDecimal;
import java.util.List;


//Nicklas s185105
public class BankService {

    private static BankService bankServiceInstance = null;
    private dtu.ws.fastmoney.BankService bank;

    private BankService(){
        bank = new BankServiceService().getBankServicePort();
    }


    public static BankService getInstance(){
        if (bankServiceInstance == null){
            bankServiceInstance = new BankService();
        }
        return bankServiceInstance;
    }

    public String createBankAccount(String first, String lastname,String cprnumber)  {
        User user = new User();
        user.setFirstName(first);
        user.setLastName(lastname);
        user.setCprNumber(cprnumber);

        String id = "";

        try {
            id = bank.createAccountWithBalance(user, new BigDecimal(0));
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
        }
        return id;
    }

    public String createBankAccountWithBalance(String first, String lastname,String cprnumber, BigDecimal balance)  {
        User user = new User();
        user.setFirstName(first);
        user.setLastName(lastname);
        user.setCprNumber(cprnumber);

        String id = "";

        try {
            id = bank.createAccountWithBalance(user, balance);
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
        }
        return id;
    }

    public void retireAccount(String accountid)  {
        try {
            bank.retireAccount(accountid);
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
        }
    }

    public BigDecimal getBalanceById(String accountid)  {
        try {
            return bank.getAccount(accountid).getBalance();
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
        }
        return new BigDecimal(1);
    }

    public boolean transferMoneyFromTo(String customerId, String merchantId, int amount, String description)  {
        try {
            bank.transferMoneyFromTo(customerId, merchantId, new BigDecimal(amount), description);
            return true;
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public List<AccountInfo> getAccounts(){
        return bank.getAccounts();
    }


}
