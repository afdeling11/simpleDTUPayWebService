import Transaction.aggregate.CorrelationId;
import Transaction.aggregate.ID;
import Transaction.aggregate.Token;
import Transaction.repositories.ReadTransactionModelRepository;
import Transaction.repositories.TransactionRepository;
import Transaction.services.TransactionService;
import Transaction.aggregate.Transaction;
import messaging.Event;
import messaging.MessageQueue;
import messaging.implementations.MessageQueueSync;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class TransactionUnitTest {
    MessageQueue queue = mock(MessageQueue.class);
    Transaction transaction;
    TransactionService transactionService;
    TransactionRepository transactionRepository;
    ReadTransactionModelRepository readTransactionModelRepository;
    Transaction expected;
    Set<Transaction> expectedTransactions;
    private CorrelationId correlationId;
    private void setup_sync_queues() {
        MessageQueue eventQueue = new MessageQueueSync();
        transactionRepository = new TransactionRepository(eventQueue);
        readTransactionModelRepository = new ReadTransactionModelRepository(eventQueue);
        transactionService= new TransactionService(transactionRepository, readTransactionModelRepository, queue);
    }
    public void applicationTest() throws InterruptedException {
        correlationId = CorrelationId.randomId();
        Token token = new Token();
        token.setToken("1234");
        ID transactionId = new ID(UUID.randomUUID());
        transaction = new Transaction();
        transaction.setId(transactionId);
        transaction.setToken(token);
        transaction.setMerchantID("4321");
        transaction.setAmount(5);
        transaction.setDescription("Test Transaction");
        transactionService.handleMerchantTransaction(new Event(TransactionService.MERCHANT_TRANSACTION_INITIATED, new Object[]{transaction, correlationId}));

        expected = transactionService.createTransaction(transactionId,token, "4321", 5, "Test Transaction");

        var event = new Event(TransactionService.MERCHANT_TRANSACTION_CREATED, new Object[] {expected, correlationId});
        //verify(queue).publish(event);
        Assert.assertEquals( 5, expected.getAmount());

        transactionService.handleCustomerTransactionsRequested(new Event(TransactionService.CUSTOMER_TRANSACTIONS_REQUESTED, new Object[]{Stream.of(transaction).collect(Collectors.toSet()), correlationId, transactionId}));
        Set<Transaction> transactions = transactionService.transactions();

        expectedTransactions = transactionService.transactionsById(transactionId);
        var eventTransactions = new Event(TransactionService.CUSTOMER_TRANSACTIONS_REPORT_GENERATED, new Object[] {Stream.of(transaction).collect(Collectors.toSet()), correlationId});
        //verify(queue).publish(eventTransactions);
        Assert.assertEquals(transactions.size(), expectedTransactions.size());
    }

    @Test
    public void runTests() throws InterruptedException {
        setup_sync_queues();
        applicationTest();
    }
}
